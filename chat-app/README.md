Demo - https://chat-socket-io-ex.herokuapp.com/

## Available Scripts

`./chat-app` - CLIENT code and CRA dev server <br>
`./chat-app/server` - SERVER NodeJS code

### RUN CLIENT APP

In the directory run `./chat-app` install packages via `yarn` or `npm install`

`npm start` or `yarn start`

Runs the app in the development client mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### RUN API SERVER

`cd ./server` and `yarn` or `npm install` then `yarn start` or `npm start`

Launches the server of socket io based app on NodeJS with DB connection;

## FEATURES

-   Ability to chose color of your name in chat
    ![1](https://uc2046c1db47644298262911fb19.previews.dropboxusercontent.com/p/thumb/AAN-sLhOy3yfeNyOOGFFbVowX0Z56QpRR-9t3BP0N_rh9CR83UQddK0lhJrs56RiJl44Xa6z9fmgxKMDlMwfMo3U8xrYv02csKxI9UFMzzLpVN3DG7xL5fMZHSLYDdSzpxRgIAMYrms1OIBo70vsF6LGevn7xguCgGr63tpfuZcv5dHQl0J6ks7t0NDay6UWlCwBc8jop-CFBj9QCsTQl3l1y0pRJORej_8bh4Pjb0BtAQ/p.png?size=1024x768&size_mode=3)

-   Global window chat - sending messages to all users that are currently online
    ![1](https://uc2d756f08b6b846a0780d5ded5a.previews.dropboxusercontent.com/p/thumb/AAOxYLQ1BhBbzjLXZ4PwFCRopJO_Y-4bA-_XRUEyw1mMm0y7ziBD1U7gsWu8N2dk6u4y8BVRaWuzw8Pb88vdXEBnxkQWXzxpytNxnDz-qBN3TPgs022l44MOcVM9I35DUjK3BjlKxcDL03wSFkZ1i95f9qVATBHC4NGcbjoOXJhGkwWbnFoOUn0sPH-yhjb0E2p2fFOdCakMlV6HR-yzTX7o3NyyeI-dXrUKKgWxEuS9dA/p.png?size=2048x1536&size_mode=3)

-   Private chat - with selected by you online user - by left click on username on the right sidebar (it will create a new window like in browser)
    ![1](https://uc0b2d2ab6d7c6f0a2675fee3624.previews.dropboxusercontent.com/p/thumb/AAN_L3ux8f4sr92f3oem-eQYM1SUoKHwIZ7tfu6bmKeRXb-YwVIGZdDzEDDQPFaiHmkJsK5EivcQ7utBNhBE79mlK-ZmAiVROfYpCDr9uHJErsoAeMU3xZKrKyBxXORdiKHlaaCJi8Q7dosn51ZxiS_PLOlJ8P1YaeIPjDCUzGVPEMUtsxetWKZ_MREgbHEPKRniRYvotI6XfdiIo009jr2AgepDtVdsgKYpJdALmaj63A/p.png?size=1024x768&size_mode=3)
    ![1](https://uc6ae81dcad913447f74f28eb584.previews.dropboxusercontent.com/p/thumb/AAMaIKEebdIB-1WoH5IrzfsM7-owr9vjcrnRsRFRVM4hfYmm3DAUo26yrsRkC2ys2sqI35aO3HvBSb235-y0iRAVwnPUow3YAKjw9oMpjeXTzxL9We4a9a4-Szks2pyX5UriTyIZ1B1Y0NB-onFTeyQD4KBUvwSoCZBJsBI14Z1JtoKUAjU8zcVZgTjmubI5RqFa17cMg_vxYTBgc2mfXpOtAiFyGK2z03FXUZ4oyZ1htQ/p.png?size=1024x768&size_mode=3)

-   Mobile Friendly
    ![1](https://uc598b648b5c84dd25c66c2da65f.previews.dropboxusercontent.com/p/thumb/AANZGTFa3O8UjrVgNdWrMoDtFKBHAnp0q_oKkBSsPaTiJLuGl5N_YRAsThI1-KTcIAPwlXseOCLBMG5UEJuotGrEkMp8K4rHCLK4QU70U60gx0WHa0JQyupjDtNWwl75X1MBSAygJzl7pzNDRdsJqdJkCy_B87A8mP8iNOieXt7OM_DWvBSoEOluCND_-Oxgw-jAKFVMyteuV3WgSSf3l3AOREyYggDEuKlyFC-cVHgsTQ/p.png?size=1024x768&size_mode=3)
